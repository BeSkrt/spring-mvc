package be.vdab.springmvcexplanation.books.controllers;

import be.vdab.springmvcexplanation.books.entities.Book;
import be.vdab.springmvcexplanation.books.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("books")
public class BookController {

    private BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping
    public String showBookList(Model model) {
        List<Book> bookList = bookRepository.getBooks();
        model.addAttribute("books", bookList);
        return "book/BookList";
    }

    @GetMapping("{isbn}")
    public String showBook(Model model, @PathVariable String isbn) {
        Book book = bookRepository.getBook(isbn);
        model.addAttribute("book", book);
        return "book/BookDetails";
    }
}
