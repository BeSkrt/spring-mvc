package be.vdab.springmvcexplanation.books.repositories;

import be.vdab.springmvcexplanation.books.entities.Book;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookRepositoryImplementation implements BookRepository{

    private Map<String, Book> books = new HashMap<>();
    private Book harryPotter = new Book("123", "Harry Potter", "J.K. Rowling", 14.99);
    private Book theOldManAndTheSea = new Book("456", "The Old Man And The Sea", "Ernest Hemingway", 9.99);

    @PostConstruct
    public void init() {
        setBooks(books);
    }

    public void setBooks(Map<String, Book> books) {
        books.put(harryPotter.getIsbn(), harryPotter);
        books.put(theOldManAndTheSea.getIsbn(), theOldManAndTheSea);
        this.books = books;
    }

    @Override
    public List<Book> getBooks() {
        return new ArrayList<>(books.values());
    }

    @Override
    public Book getBook(String isbn) {
        return books.get(isbn);
    }
}
