package be.vdab.springmvcexplanation.books.repositories;

import be.vdab.springmvcexplanation.books.entities.Book;
import java.util.List;

public interface BookRepository{

    List<Book> getBooks();
    Book getBook(String isbn);
}
