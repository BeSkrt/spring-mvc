package be.vdab.springmvcexplanation.dayOfWeek.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.DayOfWeek;

@Controller
@RequestMapping("day")
public class DayOfWeekController {

    @GetMapping
    public String handleGet(Model model) {
        model.addAttribute("daysOfWeek", DayOfWeek.values());
        return "dayOfWeek/DayOfWeekView";
    }
}
