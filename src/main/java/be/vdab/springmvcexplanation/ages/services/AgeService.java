package be.vdab.springmvcexplanation.ages.services;

import org.springframework.stereotype.Service;

@Service
public class AgeService {
    private String age;

    public AgeService() {
    }

    public AgeService(String age) {
        this.age = age;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String checkAge(String age) {
        if(age.equals("child")) {
            return "redirect:child";
        } else if (age.equals("teenager")) {
            return "redirect:teenager";
        } else if (age.equals("adult")) {
            return "redirect:adult";
        } else {
            return "age/AgeView";
        }
    }

    @Override
    public String toString() {
        return "AgeService{" +
                "age=" + age +
                '}';
    }
}
