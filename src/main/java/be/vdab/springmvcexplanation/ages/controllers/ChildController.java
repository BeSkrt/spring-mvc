package be.vdab.springmvcexplanation.ages.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("child")
public class ChildController {

    @GetMapping
    public String renderChildView(){
        return "age/children";
    }

}
