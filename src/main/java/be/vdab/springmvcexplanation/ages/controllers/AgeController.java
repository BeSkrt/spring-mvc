package be.vdab.springmvcexplanation.ages.controllers;

import be.vdab.springmvcexplanation.ages.services.AgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("age")
public class AgeController {
    private AgeService ageService;

    @Autowired
    public void setAgeService(AgeService ageService) {
        this.ageService = ageService;
    }

    @GetMapping
    public String handleAgeView() {
        return "age/AgeView";
    }

    @PostMapping
    public String renderView(@RequestParam String age) {
        return ageService.checkAge(age);
    }

//    @GetMapping(params = "action=submit")
//    public String handleSubmit(Model model) {
//        model.addAttribute("submit", ageService);
//
//        if(ageService.getAge() >-1 && ageService.getAge() < 10) {
//            return "age/children";
//        } else if(ageService.getAge() > 9 && ageService.getAge() < 20) {
//            return "age/teenagers";
//        } else if(ageService.getAge() >= 20) {
//            return "age/adults";
//        }
//        return
//    }

//    @GetMapping(params = {"age=child", "!cancel"})
//    public String handleChildren(Model model) {
//        model.addAttribute("child", ageService);
//        return "age/children";
//    }

//    @GetMapping(params = {"age=teenager", "!cancel"})
//    public String handleTeenagers(Model model) {
//        model.addAttribute("teenager", ageService);
//        return "age/teenagers";
//    }

//    @GetMapping(params = {"age=adult", "!cancel"})
//    public String handleAdults(Model model) {
//        model.addAttribute("adult", ageService);
//        return "age/adults";
//    }

    @GetMapping(params = "cancel")
    public String handleCancel(Model model) {
        return "age/cancel";
    }
}
