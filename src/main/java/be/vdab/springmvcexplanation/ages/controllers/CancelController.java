package be.vdab.springmvcexplanation.ages.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("cancel")
public class CancelController {

    @GetMapping
    public String renderCancelView(){
        return "age/cancel";
    }
}
