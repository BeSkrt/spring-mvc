package be.vdab.springmvcexplanation.ages.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("teenager")
public class TeenagerController {

    @GetMapping
    public String renderTeenagerView(){
        return "age/teenagers";
    }
}
