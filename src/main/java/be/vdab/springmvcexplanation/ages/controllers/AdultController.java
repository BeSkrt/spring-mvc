package be.vdab.springmvcexplanation.ages.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("adult")
public class AdultController {

    @GetMapping
    public String renderAdultView(){
        return "age/adults";
    }
}
