package be.vdab.springmvcexplanation.counter.services;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

@Service
@RequestScope
public class CounterServiceImplementation implements CounterService {
    private int count;

    public CounterServiceImplementation() {
    }

    public CounterServiceImplementation(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int addToCount() {
        return ++count;
    }
}
