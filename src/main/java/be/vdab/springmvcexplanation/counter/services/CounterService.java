package be.vdab.springmvcexplanation.counter.services;

public interface CounterService {

    int addToCount();
}
