package be.vdab.springmvcexplanation.counter.controllers;

import be.vdab.springmvcexplanation.counter.services.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Controller
public class CounterController {
    @Autowired
    private CounterService countService;

    @PostConstruct
    public void init() {
        System.out.println("CounterController: init()");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("CounterController: destroy()");
    }

    @GetMapping("counter")
    public String handle(Model model) {
        model.addAttribute("count", countService);
        model.addAttribute("sentence", "Count: ");
        return "counter/CounterView";
    }


}
