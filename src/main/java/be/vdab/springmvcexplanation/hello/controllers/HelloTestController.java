package be.vdab.springmvcexplanation.hello.controllers;

import be.vdab.springmvcexplanation.hello.services.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "helloTest")
public class HelloTestController {

    private Hello helloService;

    @Autowired
    public void setHelloService(Hello helloService) {
        this.helloService = helloService;
    }

    @GetMapping
    public ModelAndView handleTest() {
        String test = helloService.sayHello();
        return new ModelAndView("testTemplates/HelloTest", "test", test);
    }
}
