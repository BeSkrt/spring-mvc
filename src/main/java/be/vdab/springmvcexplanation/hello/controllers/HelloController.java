package be.vdab.springmvcexplanation.hello.controllers;

import be.vdab.springmvcexplanation.hello.services.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("hello")
public class HelloController {

    private Hello helloService;

    @Autowired
    public void setHelloService(Hello helloService) {
        this.helloService = helloService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handle() {
        String text = helloService.sayHello();
        return new ModelAndView("hello/HelloView", "text", text);
    }
}
