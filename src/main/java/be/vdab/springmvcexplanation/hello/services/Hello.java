package be.vdab.springmvcexplanation.hello.services;

public interface Hello {

    String sayHello();
}
