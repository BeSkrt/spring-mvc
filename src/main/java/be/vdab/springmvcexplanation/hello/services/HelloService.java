package be.vdab.springmvcexplanation.hello.services;

import org.springframework.stereotype.Service;

@Service
public class HelloService implements Hello {

    @Override
    public String sayHello() {
        return "Hello World!";
    }
}
