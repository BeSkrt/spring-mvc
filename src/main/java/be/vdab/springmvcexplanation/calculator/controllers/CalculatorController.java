package be.vdab.springmvcexplanation.calculator.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("calculator")
@SessionAttributes("intermediateResult")
public class CalculatorController {
    private static final String CALCULATOR_VIEW = "calculator/CalculatorView";

    @GetMapping
    public String handleGet() {
        return CALCULATOR_VIEW;
    }

    @PostMapping
    public String calculate(Model model,
                            @RequestParam double number, @RequestParam(required = false, defaultValue = "0") String action,
                             HttpSession session) {
        Object intermediateResult = session.getAttribute("intermediateResult");

        double result;

        if(intermediateResult != null) {
            result = Double.parseDouble(intermediateResult.toString());
        } else {
            result = 0;
        }

        switch (action) {
            case "+":
                result += number;
                break;
            case "-":
                result -= number;
                break;
            case "*":
                result *= number;
                break;
            case "/":
                result /= number;
                break;
            case "Clear":
                result = 0.0;
                break;
        }
//        session.setAttribute("intermediateResult", result);
        model.addAttribute("intermediateResult", result);
        return CALCULATOR_VIEW;
    }
}
