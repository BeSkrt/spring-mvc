package be.vdab.springmvcexplanation.products.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("product")
@SessionScope
public class ProductController {
    public final static String PRODUCT_VIEW = "product/ProductView";
    private List<String> products = new ArrayList<>();

    @GetMapping
    public String handleGet(Model model) {
        model.addAttribute("products", products);
        return PRODUCT_VIEW;
    }

    @PostMapping(params = "add")
    public String handleAddPost(Model model, @RequestParam(name = "product") String product) {
        products.add(product);
        model.addAttribute("products", products);
        return PRODUCT_VIEW;
    }

    @PostMapping(params = "delete")
    public String handleDeletePost(Model model, @RequestParam(name = "product") String product) {
        products.remove(product);
        model.addAttribute("products", products);
        return PRODUCT_VIEW;
    }

//    @PostMapping(params = "change")
//    public String handleChangePost(Model model, @RequestParam(name = "oldProduct") String oldProduct,
//                                   @RequestParam(name = "product") String newProduct) {
//        int newIndex = products.indexOf(oldProduct);
//        products.set(newIndex, newProduct);
//        model.addAttribute("products", products);
//        return PRODUCT_VIEW;
//    }

    @PostMapping(params = "change")
    public String handleChangeWithIdPost(Model model, @RequestParam(name = "id") int id,
                                   @RequestParam(name = "product") String newProduct) {
        products.set(id, newProduct);
        model.addAttribute("products", products);
        return PRODUCT_VIEW;
    }

}
