package be.vdab.springmvcexplanation.home.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Random;

@Controller
@RequestMapping("/")
public class HomeController {

    public static final Random random = new Random();

    @GetMapping
    public String renderHomePage(Model model){
        model.addAttribute("marginLeft", random.nextInt(1400));
        model.addAttribute("marginTop", random.nextInt(600));
        return "home/index";
    }

}
