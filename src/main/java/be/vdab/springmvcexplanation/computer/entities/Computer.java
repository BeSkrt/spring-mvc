package be.vdab.springmvcexplanation.computer.entities;

public class Computer {
    private String os;
    private int storage;
    private int ram;
    private String processor;
    private int numberOfCores;
    private int clockSpeed;
    private String owner;

    public Computer() {
    }

    public Computer(String os, int storage, int ram, String processor, int numberOfCores, int clockSpeed, String owner) {
        this.os = os;
        this.storage = storage;
        this.ram = ram;
        this.processor = processor;
        this.numberOfCores = numberOfCores;
        this.clockSpeed = clockSpeed;
        this.owner = owner;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public int getStorage() {
        return storage;
    }

    public void setStorage(int storage) {
        this.storage = storage;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public void setNumberOfCores(int numberOfCores) {
        this.numberOfCores = numberOfCores;
    }

    public int getClockSpeed() {
        return clockSpeed;
    }

    public void setClockSpeed(int clockSpeed) {
        this.clockSpeed = clockSpeed;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "os='" + os + '\'' +
                ", storage=" + storage +
                ", ram=" + ram +
                ", processor='" + processor + '\'' +
                ", numberOfCores=" + numberOfCores +
                ", clockSpeed=" + clockSpeed +
                ", owner='" + owner + '\'' +
                '}';
    }
}
