package be.vdab.springmvcexplanation.computer.controllers;

import be.vdab.springmvcexplanation.computer.entities.Computer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("pcRequirements")
public class ComputerController {
    private static final Computer COMPUTER = new Computer();

    @GetMapping
    public String showForm(Model model) {
        model.addAttribute("computer", COMPUTER);
        return "computer/ComputerForm";
    }

    @PostMapping
    public String printComputer(@ModelAttribute Computer computer) {
        System.out.println(computer);
        return "computer/ComputerForm";
    }
}
