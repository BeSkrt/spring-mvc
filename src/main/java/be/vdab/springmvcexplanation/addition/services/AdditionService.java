package be.vdab.springmvcexplanation.addition.services;

import org.springframework.stereotype.Service;

@Service
public class AdditionService {
    private float number1;
    private float number2;
    private float result;

    public AdditionService() {
    }

    public AdditionService(float number1, float number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public float getNumber1() {
        return number1;
    }

    public void setNumber1(float number1) {
        this.number1 = number1;
    }

    public float getNumber2() {
        return number2;
    }

    public void setNumber2(float number2) {
        this.number2 = number2;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public float calculateAddition(float number1, float number2) {
        return result = number1 + number2;
    }
}
