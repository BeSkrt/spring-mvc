package be.vdab.springmvcexplanation.addition.controllers;

import be.vdab.springmvcexplanation.addition.services.AdditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("addition")
public class AdditionController {
    private AdditionService additionService;

    @Autowired
    public void setAdditionService(AdditionService additionService) {
        this.additionService = additionService;
    }

    @GetMapping
    public String handle() {
        return "addition/AdditionView";
    }

    @PostMapping
    public ModelAndView renderView(@RequestParam("number1") float number1,
                                   @RequestParam("number2") float number2) {
        float result = additionService.calculateAddition(number1, number2);
        Map<String, Float> results = new HashMap<>();
        results.put("result", result);
        return new ModelAndView("addition/AdditionView", results);
    }
}
