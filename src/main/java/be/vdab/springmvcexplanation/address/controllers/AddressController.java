package be.vdab.springmvcexplanation.address.controllers;

import be.vdab.springmvcexplanation.address.entities.Address;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("address")
@SessionAttributes({"address"})
public class AddressController {
    private static final Address ADDRESS = new Address();

    @GetMapping
    public String showForm(Model model) {
        model.addAttribute("address", ADDRESS);
        return "address/AddressForm";
    }

    @PostMapping
    public String handleForm(@SessionAttribute(name = "address") Address address) {
        System.out.println(address);
        return "address/AddressView";
    }
}
