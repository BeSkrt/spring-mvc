open module be.vdab.springmvcexplanation {
//    Spring
    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.aop;

    requires spring.boot;
    requires spring.boot.autoconfigure;

    requires spring.web;
    requires spring.boot.starter.web;
    requires spring.webmvc;
    requires org.apache.tomcat.embed.core;

//    Java
    requires java.validation;
    requires java.annotation;
}